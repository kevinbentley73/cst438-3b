package cst4383b;


import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ConfigPublisher {
	
	private String exchange_name; 
	
	@Bean
	public FanoutExchange fanout() {
		return new FanoutExchange(exchange_name);
	}
	
	public ConfigPublisher (@Value("${exchange_name}") final String exchange_name) {
		this.exchange_name = exchange_name;
	}
}