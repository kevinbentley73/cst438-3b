package cst4383b.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cst4383b.domain.*;
import cst4383b.service.CityService;

@Controller
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@GetMapping("/cities/{city}")
	public String getWeather(@PathVariable("city") String cityName, Model model) {

		CityInfo cInfo = cityService.getCityInfo(cityName);
		model.addAttribute("id",cInfo.getId());
		model.addAttribute("name",cInfo.getName());
		model.addAttribute("country_name",cInfo.getCountryName());
		model.addAttribute("country_code",cInfo.getCountryCode());
		model.addAttribute("district",cInfo.getDistrict());
		model.addAttribute("population",cInfo.getPopulation());
		model.addAttribute("weather",cInfo.getTemp());
		model.addAttribute("local_time",cInfo.getTime());

		return "showcity";
	}
	
	
	@PostMapping("/cities/reservation")
	public String getSignup(@RequestParam("cityname") String cityName, 
			                @RequestParam(name = "email") String email,
			                @RequestParam(name = "level") String level,
			                Model model) {

		model.addAttribute("level",level);
		model.addAttribute("email",email);
		model.addAttribute("city",cityName);
		cityService.requestReservation(cityName, level, email);
		return "request_reservation";
	}
}