package cst4383b.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cst4383b.domain.*;

@Service
public class CityService {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private WeatherService weatherService;

	public CityService() {

	}

	public CityService(CityRepository cityRepository, CountryRepository countryRepository) {
		this.cityRepository = cityRepository;
		this.countryRepository = countryRepository;
	}

	public CityInfo getCityInfo(String cityName) {

		List<City> cities = cityRepository.findByName(cityName);
		if (cities.isEmpty()) {

			return null;
		}
		City theCity = cities.get(0);
		Country theCountry = countryRepository.findByCode(theCity.getCountryCode());
		String countryName = "Unknown";
		if (theCountry != null) {
			countryName = theCountry.getName();
		}
		// getTempAndTime
		String weatherUrl = "http://api.openweathermap.org/data/2.5/weather";
		WeatherService ws = new WeatherService(weatherUrl, "4cdc90111e0528db2d929c9090ee6e3c");
		TempAndTime tnt = ws.getTempAndTime(cityName);
		double tempF = (tnt.temp - 273.15) * 9.0 / 5.0 + 32.0;

		Date date = new java.util.Date(tnt.time * 1000L);
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("hh:mm:ss a");
		String a[] = TimeZone.getAvailableIDs(tnt.timezone * 1000);
		if (a.length > 0) {
			sdf.setTimeZone(TimeZone.getTimeZone(a[0]));
		}
		String formattedDate = sdf.format(date);
		CityInfo info = new CityInfo(theCity, countryName, tempF, formattedDate);
		return info;
	}

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private FanoutExchange fanout;

	public void requestReservation(String cityName, String level, String email) {
		String msg = "{\"cityName\": \"" + cityName + "\" \"level\": \"" + level + "\" \"email\": \"" + email + "\"}";
		System.out.println("Sending message:" + msg);
		rabbitTemplate.convertSendAndReceive(fanout.getName(), "", // routing key none.
				msg);
	}

}
