package cst438hw2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import cst4383b.controller.CityRestController;
import cst4383b.domain.*;
import cst4383b.service.CityService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(CityRestController.class)
public class CityRestControllerTest {

	@MockBean
	private CityService cityService;

	@Autowired
	private MockMvc mvc;

	// This object will be magically initialized by the initFields method below.
	private JacksonTester<CityInfo> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, new ObjectMapper());
	}
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void getCityInfo() throws Exception {
		
		CityInfo attempt = new CityInfo(99, "Boise", "United States", "USA", "Idaho", 20001, 35, "10:33 AM");
		CityInfo expected1 = new CityInfo(99, "Boise", "United States", "USA", "Idaho", 20001, 35, "10:33 AM");
		given(cityService.getCityInfo(attempt.getName())).willReturn(attempt);
		MockHttpServletResponse response = mvc.perform(
                get("/api/cities/Boise").contentType(MediaType.APPLICATION_JSON)
                        .content(json.write(attempt).getJson()))
                .andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		String contentStr = response.getContentAsString();
		String expectedStr = json.write(expected1).getJson();
		assertThat(contentStr).isEqualTo(expectedStr);
	}

}
