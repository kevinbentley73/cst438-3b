package cst438hw2.service;
 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import cst4383b.domain.*;
import cst4383b.service.CityService;
import cst4383b.service.WeatherService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
 
@SpringBootTest
public class CityServiceTest {

	@Mock
	private WeatherService weatherService;
	
	private CityService cityService;
	
	@Mock
	private CityRepository cityRepository;
	
	@Mock
	private CountryRepository countryRepository;

	
	@Test
	public void contextLoads() {
	}


    @BeforeEach
    public void setUpEach() {
    	MockitoAnnotations.initMocks( this);
    	cityService = new CityService(cityRepository,countryRepository);
    	
    }
	
	@Test
	public void testCityFound() throws Exception {

		City expected = new City(99,"Boise","USA","Idaho",100001);
		List<City> expectedList = new ArrayList<City>();
		expectedList.add(expected);
		given(cityRepository.findByName("Boise")).willReturn(expectedList);
		
		Country country = new Country("USA","United States");
		given(countryRepository.findByCode("USA")).willReturn(country);
		
		given(weatherService.getTempAndTime("Boise")).willReturn(new TempAndTime(273, 11155577, 0));
		
		CityInfo actualCity = cityService.getCityInfo("Boise");
		assertThat(actualCity.getCountryCode()).isEqualTo("USA");
		assertThat(actualCity.getCountryName()).isEqualTo("United States");
		assertThat(actualCity.getDistrict()).isEqualTo("Idaho");
		assertThat(actualCity.getPopulation()).isEqualTo(100001);	
		
	}
	
	@Test 
	public void  testCityNotFound() {
		City expected = new City(99,"Boise","USA","Idaho",100001);
		List<City> expectedList = new ArrayList<City>();
		expectedList.add(expected);
		given(cityRepository.findByName("Boise")).willReturn(expectedList);
		
		Country country = new Country("USA","United States");
		given(countryRepository.findByCode("USA")).willReturn(country);
		
		given(weatherService.getTempAndTime("Orlando")).willReturn(new TempAndTime(273, 11155577, 0));
		
		CityInfo actualCity = cityService.getCityInfo("Orlando");
		assertThat(actualCity).isNull();
	}
	
	@Test 
	public void  testCityMultiple() {
		City expected1 = new City(99,"Boise","USA","Idaho",100001);
		City expected2 = new City(100,"Boise","USA","Texas",100);
		List<City> expectedList = new ArrayList<City>();
		expectedList.add(expected1);
		expectedList.add(expected2);
		given(cityRepository.findByName("Boise")).willReturn(expectedList);
		
		Country country = new Country("USA","United States");
		given(countryRepository.findByCode("USA")).willReturn(country);		
		given(weatherService.getTempAndTime("Boise")).willReturn(new TempAndTime(273, 11155577, 0));
		
		CityInfo actualCity = cityService.getCityInfo("Boise");
		assertThat(actualCity.getCountryCode()).isEqualTo("USA");
		assertThat(actualCity.getCountryName()).isEqualTo("United States");
		assertThat(actualCity.getDistrict()).isEqualTo("Idaho");
		assertThat(actualCity.getPopulation()).isEqualTo(100001);	
		
		
	}

}
